#pragma once
/*
* This struct is used to store a single state that contains many MopItems, for when reading from a MOP file.
*/
#include "mopItem.h"
#include "mopItemUtils.h"
struct mopState {
  mopItem * content;
  int size;
  int fSize;
  int fPos;
  double scaler;
};

