/**
* This class is used to store a single MOP file entry, which represents the state of a particle, in abbreviated form for larger file sizes
*/
#pragma once
#include <string>
/**
* This struct represents a single particle recorded to the mopfile.
*/
struct mopItem {
 
#ifdef MOPFILE
    float mass;
    float radius;
    //position in the WCS
    float x;
    float y;
    float z;
    //scaled position in the WCS
    float xs;
    float ys;
    float zs;
    //Velocity in the WCS
    float xv;
    float yv;
    float zv;
    // the colour of a particle
    int r;
    int g;
    int b;
    std::string name; // name of particle
    int type;   // used to tell this particle what type it is (collapsor/normal particle/placemark)
    int size; // what shape/size this particle will have when displayed in a visualisation

    
#endif


#ifdef MOPFILELIGHT
    int id;
    int size; // what shape/size this particle will have when displayed in a visualisation
    // the colour of a particle
    int r;
    int g;
    int b;
    //position in the WCS
    double x;
    double y;
    double z;
    //scaled position in the WCS
    float xs;
    float ys;
    float zs; 
#endif

};
